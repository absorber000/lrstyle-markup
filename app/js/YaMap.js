var map = document.getElementById('map');
var panorama = document.getElementById('panorama');


    ymaps.ready(init);

    var myMap;

    function init() {
        myMap = new ymaps.Map("map", {
            center: [55.69020592114315, 37.42189067753947],
            zoom: 18,
            controls: ['zoomControl']
        });
        myMap.behaviors.disable('scrollZoom');
        if (window.innerWidth <= 768) {
            myMap.behaviors.disable('drag');
        }
        myGeoObject = new ymaps.GeoObject();
        myMap.geoObjects
            .add(new ymaps.Placemark([55.69020592114315, 37.42189067753947], {
                balloonContent: 'ул. Рябиновая, 34а'
            }, {
                // Опции.
                // Необходимо указать данный тип макета.
                iconLayout: 'default#image',
                // Своё изображение иконки метки.
                iconImageHref: 'images/svg-icons/map-marker.svg',
                // Размеры метки.
                iconImageSize: [50, 60],
                // Смещение левого верхнего угла иконки относительно
                // её "ножки" (точки привязки).
                // iconImageOffset: [-80, -254]
            }));
        window.addEventListener("resize", function() {
            myMap.container.fitToViewport();
        });
    }


if (panorama) {
    ymaps.ready(function () {
        // Для начала проверим, поддерживает ли плеер браузер пользователя.
        if (!ymaps.panorama.isSupported()) {
            // Если нет, то просто ничего не будем делать.
            return;
        }

        // Ищем панораму в переданной точке.
        ymaps.panorama.locate([55.733685, 37.588264]).done(
            function (panoramas) {
                // Убеждаемся, что найдена хотя бы одна панорама.
                if (panoramas.length > 0) {
                    // Создаем плеер с одной из полученных панорам.
                    var player = new ymaps.panorama.Player(
                        'panorama',
                        // Панорамы в ответе отсортированы по расстоянию
                        // от переданной в panorama.locate точки. Выбираем первую,
                        // она будет ближайшей.
                        panoramas[0],
                        // Зададим направление взгляда, отличное от значения
                        // по умолчанию.
                        { direction: [256, 16], scrollZoomBehavior : false}
                    );
                }
            }
        );

    });
}





