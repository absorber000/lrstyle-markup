;(function(win, doc) {
    'use strict';

    if (window.jQuery) { var Vel = $.Velocity; } else { var Vel = Velocity; }

    class Custom {
        constructor(name) {
            this.name = name;
        }

        wrapElement(element, wrapper) {
            wrapper = wrapper || doc.createElement('div');
            if (element.nextSibling) {
                element.parentNode.insertBefore(wrapper, element.nextSibling);
            } else {
                element.parentNode.appendChild(wrapper);
            }
            return wrapper.appendChild(element);
        }

        insertAfter(elem, refElem) {
            var parent = refElem.parentNode;
            var next = refElem.nextSibling;
            if (next) {
                return parent.insertBefore(elem, next);
            } else {
                return parent.appendChild(elem);
            }
        }

        getSiblings(elem) {
            var siblings = [];
            var sibling = elem.parentNode.firstChild;
            for (; sibling; sibling = sibling.nextSibling) {
                if (sibling.nodeType === 1 && sibling !== elem) {
                    siblings.push(sibling);
                }
            }
            return siblings;
        }
    }

    class CSelect extends Custom {

        constructor(el, details, setActiveItemCallback) {
            super();
            this.store = {};
            this.details = details;
            this.store.select = el;
            this.details.setActiveItemCallback = setActiveItemCallback;
            this.init();
            this.addEventListeners();
            this.par = details.par;
        }

        getLabel(value) {
            let res;
            for (let i = 0; i < this.store.choicesLength; i++) {
                let el = this.details.choices[i];
                if (el.value === value) {
                    res = el.label;
                    this.store.activeEl = this.store.arrElms[i];
                    this.store.arrElms[i].classList.add("selectize__item--active");
                    break;
                }
            }
            return res;
        }

        createSelectizeList(arr, dest) {
            let arrLength = arr.length;
            this.store.arrElms = [];
            for (let i = 0; i < arrLength; i++) {
                let el = arr[i];
                let listEl = doc.createElement('div');
                listEl.classList.add("selectize__item");
                this.store.arrElms.push(listEl);
                listEl.innerHTML = el.label;
                listEl.setAttribute('data-selectize-item', i);
                dest.appendChild(listEl);
            }
        }


        setActiveItem(el) {
            const prevActiveEl = this.store.activeEl;
            const thisAttr = el.getAttribute('data-selectize-item');
            this.store.activeEl = el;
            el.classList.add("selectize__item--active");
            prevActiveEl.classList.remove("selectize__item--active");
            this.store.singleOption.innerHTML = this.details.choices[thisAttr].label;
            this.store.singleOption.value = this.details.choices[thisAttr].value;
            this.store.single.innerHTML = this.details.choices[thisAttr].label;
            this.dropdownClose();
            if (this.details.setActiveItemCallback) {
                this.details.setActiveItemCallback();
            }
        }

        renderChoices(arr) {
            const listArr = this.store.list.childNodes;
            const prevArrLength = listArr.length;
            const thisArrLength = arr.length;
            this.details.choices = arr;
            for (let j = 0; j < prevArrLength; j++) {
                let el = listArr[j];
                this.store.arrElms.shift();
            }
            this.store.list.parentNode.removeChild(this.store.list);
            this.store.list = doc.createElement('div');
            this.store.list.classList.add("selectize__list");
            this.store.list.classList.add("selectize__list--dropdown");
            this.store.selectize.appendChild(this.store.list);
            for (let i = 0; i < thisArrLength; i++) {
                let el = arr[i];
                let listEl = doc.createElement('div');
                listEl.classList.add("selectize__item");
                //this.store.arrElms.shift();
                this.store.arrElms.unshift(listEl);
                listEl.innerHTML = el.label;
                listEl.setAttribute('data-selectize-item', i);
                this.store.list.appendChild(listEl);
                if (i === 0) {
                    listEl.classList.add("selectize__item--active");
                    this.store.activeEl.classList.remove("selectize__item--active");
                    this.store.activeEl = listEl;
                }
            }
            this.store.single.innerHTML = arr[0].label;
            this.store.singleOption.innerHTML = arr[0].label;
            this.store.singleOption.value = arr[0].value;
        }

        dropdownOpen() {
            this.store.list.classList.add("is-active");
            this.store.hasActiveDropdown = true;
        }

        dropdownClose() {
            this.store.list.classList.remove("is-active");
            this.store.hasActiveDropdown = false;
        }

        _onClick(e) {
            const target = e.target;

            if (this.store.selectize.contains(target)) {
                console.log(this.store.hasActiveDropdown);
                console.log(typeof target);
                if (target.classList.contains("selectize__list--single") && !this.store.hasActiveDropdown) {    // открытие дропдауна
                    this.dropdownOpen();
                }
                else if (target.classList.contains("selectize__list--single") && this.store.hasActiveDropdown){
                    this.dropdownClose();
                }
                if (target.classList.contains("selectize__item") && !target.classList.contains("selectize__item--active")) {
                    target.classList.add("selectize__item--active");
                    this.setActiveItem(target);
                }
                else if (target.classList.contains("selectize__item") && target.classList.contains("selectize__item--active")) {
                    this.dropdownClose();
                }
            }
            else {
                if (this.store.hasActiveDropdown) {
                    this.dropdownClose();
                }
            }
        }

        addEventListeners() {
            doc.addEventListener('click', event => this._onClick(event));
            // doc.addEventListener('click', event => this.onMouseDown(event));
        }

        init() {
            this.store.single = doc.createElement('div');
            this.store.inner = doc.createElement('div');
            this.store.selectize = doc.createElement('div');
            this.store.list = doc.createElement('div');

            //__________________________________________________________-
            this.store.hasActiveDropdown = false;
            //__________________________________________________________-

            // let selectOptionArray = this.el.querySelectorAll("option");
            this.createSelectizeList(this.details.choices, this.store.list);
            this.store.select.classList.add("is-hidden");
            this.store.choicesLength = this.details.choices.length;

                this.store.single.innerHTML = this.details.choices[0].label;
                this.store.singleOption = doc.createElement('option');
                this.store.singleOption.innerHTML = this.details.choices[0].label;
                this.store.singleOption.value = this.details.selected;
                this.store.select.appendChild(this.store.singleOption);

            this.store.selectize.classList.add("selectize");
            if (this.details.subclass) {
                this.store.selectize.classList.add(this.details.subclass);
            }
            this.store.list.classList.add("selectize__list");
            this.store.list.classList.add("selectize__list--dropdown");
            this.store.single.classList.add("selectize__list");
            this.store.single.classList.add("selectize__list--single");
            this.store.inner.classList.add("selectize__inner");
            super.wrapElement(this.store.select, this.store.inner);
            super.wrapElement(this.store.inner, this.store.selectize);
            super.insertAfter(this.store.list, this.store.inner);
            super.insertAfter(this.store.single, this.store.select);

        }

    }

    class CAccordeon extends Custom {
        constructor(el, details) {
            super();
            this.store = {};
            this.details = details;
            this.store.acc = el;
            this.init();
            this.addEventListeners();
        }

        toggle(el) {
            const attrTitle = el.getAttribute('data-acc-title');
            const thisBlock = this.store.blocks[attrTitle];
            let content = this.store.arrWrappers[attrTitle];
            if (this.details.multiple) {
                if (!el.classList.contains(this.details.clsactive)) {
                    el.classList.add(this.details.clsactive);
                    thisBlock.classList.add(this.details.clsactive);
                    Vel(content, "slideDown", {duration: 300}, {easing: "none"}, {
                        complete: function() {
                            content.classList.add("is-hidden");
                            el.classList.remove(this.details.clsactive);

                        }
                    }, {
                        begin: function() {
                            content.classList.remove("is-hidden");
                        }
                    });
                }
                else if (el.classList.contains(this.details.clsactive)) {
                    el.classList.remove(this.details.clsactive);
                    thisBlock.classList.remove(this.details.clsactive);
                    Vel(content, "slideUp", {duration: 300}, {easing: "none"}, {complete: function() {console.log("complete");}});
                }
            }
        }

        _onClick(e) {
            let target = e.target;
            if (this.store.acc.contains(target)) {
                if (win.innerWidth > 1024 && this.details.flag) {
                    return;
                }
                while (target.classList !== this.details.toggle) {
                    if (target.classList.contains(this.details.toggle)) {
                        this.toggle(target);
                        e.preventDefault();
                        return;
                    }
                    target = target.parentNode;
                }
            }
        }

        addEventListeners() {
            doc.addEventListener('click', event => this._onClick(event));
        }

        destroy() {
            for (let i = 0; i < this.store.arrWrappers.length; i++) {
                let el = this.store.arrWrappers[i];
                let elT = this.store.arrTitles[i];
                el.classList.remove('is-hidden');
                elT.classList.remove(this.details.clsactive);
                el.style.display = "block";
            }
        }

        finit() {
            for (let i = 0; i < this.store.arrWrappers.length; i++) {
                let el = this.store.arrWrappers[i];
                el.classList.add('is-hidden');
                el.style.display = "none";
            }
        }

        init() {
            this.store.arrTitles = [];
            this.store.arrContainers = [];
            this.store.blocks = [];
            this.store.arrWrappers = [];
            const titles = this.store.acc.querySelectorAll("." + this.details.toggle);
            const containers = this.store.acc.querySelectorAll("." + this.details.containers);
            const blocks = this.store.acc.querySelectorAll("." + this.details.blocks);
            const titlesLength = titles.length;
            for (let i = 0; i < titlesLength; i++) {
                let titlesEl = titles[i];
                let containersEl = containers[i];
                let blockEl = blocks[i];
                blockEl.setAttribute('data-acc-block', i);
                let wrapper = doc.createElement('div');
                this.store.arrTitles.push(titlesEl);
                this.store.arrContainers.push(containersEl);
                this.store.arrWrappers.push(wrapper);
                this.store.blocks.push(blockEl);
                titlesEl.setAttribute('data-acc-title', i);
                containersEl.setAttribute('data-acc-container', i);

                wrapper.setAttribute('data-acc-wrapper', i);
                wrapper.classList.add("acc-wrapper", "is-hidden");
                super.wrapElement(containersEl, wrapper);
            }
        }
    }

    class CHeader extends Custom {
        constructor(el, details) {
            super();
            this.details = details;
            this.details.header = el;
            this.init();
            this.addEventListeners();
        }

        setMatchHeight() {
            this.details.sitewrapper.style.paddingTop = this.details.header.clientHeight + "px";
        }

        init() {
            this.setMatchHeight();
        }

        _onResize() {
            this.setMatchHeight();

        }

        addEventListeners() {
            win.addEventListener('resize', event => this._onResize());
        }

    }

    class CDropdown extends Custom {
        constructor(e) {
            super();
            // this.details = details;
            // this.details.header = el;
            this.init();
            this.addEventListeners();
        }

        _onClick(e) {

        }

        addEventListeners() {
            doc.addEventListener('click', event => this._onClick());
        }

        init() {
            let el = doc.querySelectorAll('[data-dropdown]')[0];
        }
    }

    class CModelChecker extends Custom {
        constructor(list, selEngine, selYear, models, clickF) {
            super();
            this.list = list;
            this.selEngine = selEngine;
            this.selYear = selYear;
            this.models = models;
            this.clickF = clickF;
            this.init();
            this.addEventListeners();
        }

        _onClick(e) {
            const target = e.target;
            if (this.list.contains(target) && target.classList.contains("tech-serv__model-checker-item")) {
                this.clickF(e);
                if (target !== this.activeEl) {
                    target.classList.add("active");
                    this.activeEl.classList.remove("active");
                    this.activeEl = target;
                }
            }
        }


        addEventListeners() {
            doc.addEventListener('click', event => this._onClick(event));
        }

        init() {
            let counter = 0;
            let row;
            for (var key in this.models) {
                let item = doc.createElement('div');
                item.classList.add("tech-serv__model-checker-item");
                // item.setAttribute('data-value', this.models)
                item.innerHTML = this.models[counter].label;
                item.setAttribute('data-checker-id', counter);
                if (counter === 0) {
                    item.classList.add("active");
                    this.activeEl = item;
                }
                if (counter % 3 === 0) {
                    row = doc.createElement('div');
                    row.classList.add("tech-serv__model-checker-row");
                    this.list.appendChild(row);
                }
                row.appendChild(item);
                counter++;
            }
        }
    }

    function modelsAcc() {
        var acc = doc.getElementById("models-acc");
        var modelsAcc;
        var flag = false;
        var first = false;
        if (!acc) {
            return;
        }

        function doit() {
            if (win.innerWidth <= 1024 && !flag && !first) {
                modelsAcc = new CAccordeon(acc, {
                    showfirst: false,
                    multiple: true,
                    animate: false,
                    duration: 300,
                    toggle: "page-index__models-acc-title",
                    containers: "page-index__models-acc-content",
                    blocks: "page-index__models-acc-item",
                    flag: true,
                    clsactive: "is-active"
                });
                first = true;
                flag = true;
            }
            if (win.innerWidth <= 1024 && !flag) {
                modelsAcc.finit();
                flag = true;
            }
            else if (win.innerWidth > 1024 && flag) {
                modelsAcc.destroy();
                flag = false;
            }
        }

        doit();
        win.addEventListener('resize', doit);
    }

    function ArrayFromModels(obj) {
        let arr = [];
        for (var key in obj) {
            arr.push(obj[key]);
            let engines = [];
            for (var key1 in obj[key].engines) {
                engines.push(obj[key].engines[key1]);
                let years = [];
                for (var key2 in obj[key].engines[key1].years) {
                    years.push(obj[key].engines[key1].years[key2]);
                }
                obj[key].engines[key1].years = years;
            }
            obj[key].engines = engines;
        }
        return arr;
    }

    doc.addEventListener("DOMContentLoaded", function() {
        const carEngine = doc.getElementById("car-engine");
        const carYear = doc.getElementById("car-year");
        const acc = doc.getElementById("to-accordion");
        const header = doc.getElementById("page-header");
        const siteWrapper = doc.getElementById("site-wrapper");
        const modelsInput = doc.querySelector('.tech-serv__models-input');
        var stickyEl = $("#sidebar-sticky-part");
        stickyEl.stick_in_parent()

            .on("sticky_kit:stick", function(e) {
                stickyEl.addClass("stick");
            })
            .on("sticky_kit:bottom", function(e) {
                stickyEl.addClass("unstick");
            })
            .on("sticky_kit:unbottom", function(e) {
                    stickyEl.removeClass("unstick");
            });
        if (models){
            models = ArrayFromModels(models);
            if (modelChecker) {
                var modelCheckerObj = new CModelChecker(modelChecker, carEngine, carYear, models, function(e) {
                    const target = e.target;
                    const id = target.getAttribute('data-checker-id');
                    selEngines.dropdownClose();
                    selYears.dropdownClose();
                    selEngines.renderChoices(models[id].engines);
                    selYears.renderChoices(models[id].engines[0].years);
                    modelsInput.setAttribute("value", models[id].value);
                });
                modelsInput.setAttribute("value", models[0].value);
            }
        }

        if (carYear) {
            var selYears = new CSelect(carYear, {
                choices: models[0].engines[0].years,
                selected: "0",
                subclass: "selectize--dark"
            });
        }

        if (carEngine) {
            var selEngines = new CSelect(carEngine, {
                choices: models[0].engines,
                selected: "1",
                subclass: "selectize--dark"
            }, function() {
                var id = selEngines.store.activeEl.getAttribute("data-selectize-item");
                var tabId = modelCheckerObj.activeEl.getAttribute("data-checker-id");
                selYears.renderChoices(models[tabId].engines[id].years)
            });
        }
        if (acc) {
            var toAcc = new CAccordeon(acc, {
                showfirst: false,
                multiple: true,
                animate: false,
                duration: 300,
                toggle: "to-accordion__title",
                containers: "to-accordion__content",
                blocks: "to-accordion__block",
                clsactive: "is-active"
            });
        }
        modelsAcc();
        var Header = new CHeader(header, {
            sitewrapper: siteWrapper
        });
        var dd = new CDropdown();

        let headerNavbar = doc.getElementById("header-navbar");

    });
})(window, document);


;(function(win, doc, $) {
    'use strict';

    if (window.jQuery) { var Vel = $.Velocity; } else { var Vel = Velocity; }

    var body = doc.querySelectorAll("body")[0];
    var html = doc.querySelectorAll("html")[0];
    var header = doc.querySelector("#page-header");
    var logoPanel = $(".logo-panel");
    var searchBar = $("#search-bar");
    var navbar = $(".page-header__navbar");
    var headerLogo = $(".logo-pannel__logo img");
    var overlay = $('#site-wrapper-overlay');
    var overlay2 = $('#site-wrapper-overlay2');
    var headerOverlay = $("#page-header-overlay");
    var searchBarInput = $(".searchbar__input input");
    var mobileNavbar = $(".mobile-navbar");
    var navbarPinnedFlag = false;
    var navbarPinnedFlagFirst = false;
    var navPinnedFlag = false;
    var autoHeightFlag = false;
    var siteWrapper = $(".site-wrapper");
    var Header = $(".page-header");
    var b = $('body');
    var bWidth = b.outerWidth();
    var scrollbar = window.innerWidth - bWidth;

    function overlayIt() {
        overlay.stop().fadeIn(200, function() {
            overlay.addClass("open");
        });
    }

    function overlayOver() {
        overlay.stop().fadeOut(200, function() {
            overlay.removeClass("open");
        });
    }

    function headerOverlayIt() {
        headerOverlay.stop().fadeIn(150, function() {
            headerOverlay.addClass("open");
        });
    }

    function headerOverlayOver() {
        headerOverlay.stop().fadeOut(150, function() {
            headerOverlay.removeClass("open");
        });
    }

    // $(".page-header__number-toggle").trigger('openIt');
    function slideOut() {
        var slideout = new Slideout({
            'panel': document.getElementById('site-wrapper'),
            'menu': document.getElementById('slide-menu'),
            'padding': 268,
            'tolerance': 70
        });
        var toggle = document.getElementById('js-mobile-menu-toggle');
        slideout.disableTouch();
        var offcanvas = document.getElementById('offcanvas');
        var wrapper = document.getElementById('site-wrapper');
        var btnFilter = document.querySelectorAll(".btn-filter")[0];
        var mobileFilter = document.getElementById('mobile-filter');
        var overlay = document.getElementById('site-wrapper-overlay');
        var slideMenuClose = $(".slide-menu__close");
        var b = $('body');
        var open = false;
        slideMenuClose.css({"left": "-310px"});
        var jumpFix = function() {
            var bWidth = b.outerWidth();
            var scrollbar = window.innerWidth - bWidth;

            slideout.on('beforeopen', function() {
                overlayIt();

                if (window.innerWidth - bWidth != 0) {
                    b.css({"padding-right": scrollbar});
                }
            });

            slideout.on('close', function() {
                overlayOver();
                b.css({"padding-right": "0"});
            });
        };

        function toggleClose() {
            if (open) {
                slideMenuClose.velocity({
                    properties: {left: "-310px"},
                    options: {duration: 250},
                });
                open = false;
            }
            else {
                slideMenuClose.velocity({
                    properties: {left: "268px"},
                    options: {duration: 220}
                });
                open = true;
            }
        }

        slideMenuClose.click(function() {
            toggleClose();
            slideout.toggle();
        });


        toggle.addEventListener('click', function() {
            toggleClose();
            slideout.toggle();
        });


        jumpFix();

        win.addEventListener("resize", function() {
            jumpFix();
        });
    }

    function searchBarF() {
        var searchBarCloseEl = $(".searchbar__close");
        var searchBarBtn = $(".logo-panel__searchBtn");
        var mobileSearchTrigger = $(".mobile-navbar__search");

        function searchBarOpen() {
            if (win.innerWidth <= 1024) {
                searchBar.stop().slideDown(150, function() {
                    searchBar.addClass("searchbar--visible");
                });
            }
            else {
                searchBar.stop().fadeIn(150, function() {
                    searchBar.addClass("searchbar--visible");
                });
            }
        }

        function searchBarClose() {
            if (win.innerWidth <= 1024) {
                searchBar.stop().slideUp(150, function() {
                    searchBar.removeClass("searchbar--visible");
                });
            }
            else {
                searchBar.stop().fadeOut(150, function() {
                    searchBar.removeClass("searchbar--visible");
                });
            }
        }

        mobileSearchTrigger.click(function() {
            if (!mobileSearchTrigger.hasClass("active")) {
                searchBarOpen();
                overlay2.stop().fadeIn(150, function() {
                    overlay2.addClass("open");
                });
                mobileNavbar.addClass("searchbar-open");
                mobileSearchTrigger.addClass("active");
            }
            else if (mobileSearchTrigger.hasClass("active")) {
                searchBarClose();
                overlay2.stop().fadeOut(150, function() {
                    overlay2.removeClass("open");
                });
                mobileNavbar.removeClass("searchbar-open");
                mobileSearchTrigger.removeClass("active");
            }
        });
        searchBarCloseEl.click(function() {
            headerOverlayOver();
            overlayOver();
            overlay2.stop().fadeOut(150, function() {
                overlay2.removeClass("open");
            });
            searchBarClose();
            mobileSearchTrigger.removeClass("active");
        });
        overlay2.click(function() {
            overlay2.stop().fadeOut(150, function() {
                overlay2.removeClass("open");
            });
            headerOverlayOver();
            searchBarClose();
            mobileSearchTrigger.removeClass("active");
        });
        searchBarBtn.click(function() {
            searchBarOpen();
            headerOverlayIt();
            overlayIt();
            headerOverlay.click(function() {
                headerOverlayOver();
                overlayOver();
                searchBarClose();
            });
            overlay.click(function() {
                overlayOver();
                headerOverlayOver();
                searchBarClose();
            });
        });

    }

    function dropdown() {
        var pageMain = doc.querySelector(".page-main");
        var pageFooter = doc.querySelector(".page-main");
        $('[data-uk-dropdown]').on('show.uk.dropdown', function() {
            if (win.innerWidth > 480) {
                overlay2.stop().fadeIn(150, function() {
                    overlay2.addClass("open");
                });
            }

        });
        $('[data-uk-dropdown]').on('hide.uk.dropdown', function() {
            if (win.innerWidth > 480) {
                overlay2.stop().fadeOut(150, function() {
                    overlay2.removeClass("open");
                });
            }
        });
    }

    function carousels() {
        var associates = $(".js-associates-carousel");
        var sertificates = $(".sertificates-gallery__inner");
        var stocks = $(".page-index__stock-list");
        var articleSlider = $(".article__slider");
        var delFlag = false;
        var delFlag2 = true;
        var stocksFlag = true;
        var articleCarouselNext = $(".article__slider.owl-carousel .owl-next");
        var articleCarouselPrev = $(".article__slider.owl-carousel .owl-prev");

        function doIt2() {
            if (win.innerWidth <= 480) {
                delFlag2 = false;
            }
            if (win.innerWidth > 480) {
                delFlag2 = true;
            }
            if (win.innerWidth <= 480 && !(associates.hasClass("owl-carousel")) && !(delFlag2)) {
                createSerfs();
                sertificates.addClass("owl-carousel");
            }
            else if (win.innerWidth > 480 && (delFlag2)) {
                sertificates.trigger("destroy.owl.carousel");
                sertificates.removeClass("owl-carousel");
            }
        }

        function createSerfs() {
            sertificates.owlCarousel({
                items: 1,
                dots: false,
                nav: true,
                autoplay: false,
                loop: true,
                margin: 20
            });
        }

        articleSlider.owlCarousel({
            items: 1,
            dots: false,
            nav: true,
            autoplay: false,
            loop: true,
            margin: 0
        });

        function stocksCreate() {
            stocks.owlCarousel({
                items: 3,
                dots: false,
                nav: true,
                autoplay: false,
                loop: true,
                margin: 40,
                responsive: {
                    0: {
                        items: 1,
                    },

                    570: {
                        items: 2,
                    },

                    850: {
                        items: 3,
                    }
                }
            });
        }

        stocksCreate();

        function create() {
            associates.owlCarousel({
                items: 3,
                dots: false,
                nav: true,
                autoplay: false,
                loop: true,
                margin: 20,
                responsive: {
                    0: {
                        items: 1,
                    },

                    570: {
                        items: 2,
                    },

                    850: {
                        items: 3,
                    },

                    1100: {
                        items: 4,
                    }
                }
            });
        }

        function doIt() {
            if (win.innerWidth <= 480) {
                delFlag = true;
            }
            if (win.innerWidth > 480) {
                delFlag = false;
            }
            if (win.innerWidth <= 767 && stocksFlag) {
                stocks.trigger("destroy.owl.carousel");
                stocks.removeClass("owl-carousel");
                stocksFlag = false;
            }
            if (win.innerWidth > 767 && !stocksFlag) {
                stocks.addClass("owl-carousel");
                stocksCreate();
                stocksFlag = true;
            }

            if (win.innerWidth <= 480 && associates.hasClass("owl-carousel") && (delFlag)) {
                associates.trigger("destroy.owl.carousel");
                associates.removeClass("owl-carousel");

            }
            else if (win.innerWidth > 480 && !(delFlag)) {
                create();
                associates.addClass("owl-carousel");
            }
        }

        doIt();
        doIt2();

        function sliderMatchHeight() {
            var activeImg = articleSlider.find(".owl-item.active").find(".article__slider-item-img");
            if (articleCarouselNext.outerHeight() != activeImg.outerHeight()) {
                articleCarouselNext.css({"height": activeImg.outerHeight()});
                articleCarouselPrev.css({"height": activeImg.outerHeight()});
            }
        }

        sliderMatchHeight();
        articleSlider.on('translate.owl.carousel', function(event) {
            sliderMatchHeight();
        });
        win.addEventListener("resize", function() {
            associates.trigger('refresh.owl.carousel');
            doIt();
            sliderMatchHeight();
            doIt2();
        });
    }

    function sandwitchDD() {
        var parent = $(".slide-menu__nav-parent");
        var link = $(".slide-menu__nav-parent > a");
        var ddheader = $(".slide-menu__nav-submenu-header");

        link.click(function(event) {
            var dropdown = $(this).siblings(".slide-menu__nav-submenu");
            if (!dropdown.hasClass("active")) {
                dropdown.addClass("active");
            }
            event.preventDefault();
        });
        ddheader.click(function() {
            $(this).parents(".slide-menu__nav-submenu").removeClass("active");
        });
    }

    function mobileCardTitle() {
        var tablet = false;
        var target = $(".b-product__title");
        var mobileContainer = $(".b-product__mobile-title-wrapper");
        var desktopContainer = $(".b-product__desktop-title-wrapper");

        function tabletFlug() {
            if (win.innerWidth <= 959) {
                tablet = true;
            }
            else if (win.innerWidth > 959) {
                tablet = false;
            }
            if (tablet) {
                target.prependTo(mobileContainer);
            }
            else if (!tablet) {
                target.prependTo(desktopContainer);
            }
        }

        tabletFlug();
        win.addEventListener("resize", function() {
            tabletFlug();
        });
    }

    function adaptiveSliderSpares() {
        var slider = $(".adaptive-spares-slider");
        var slider2 = $(".adaptive-spares-slider2");
        var itsSliderFlag = false;

        function doIT() {
            if (win.innerWidth <= 479 && !itsSliderFlag) {
                slider.addClass("owl-carousel");
                slider2.addClass("owl-carousel");
                slider.owlCarousel({
                    items: 1,
                    dots: true,
                    nav: true,
                    autoplay: false,
                    loop: true,
                    margin: 0
                });
                slider2.owlCarousel({
                    items: 3,
                    dots: true,
                    nav: false,
                    autoWidth: true,
                    autoplay: false,
                    loop: true,
                    margin: 10
                });
                itsSliderFlag = true;
                slider.trigger('refresh.owl.carousel');
                slider2.trigger('refresh.owl.carousel');
            }
            else if (win.innerWidth > 479 && itsSliderFlag) {
                slider.trigger("destroy.owl.carousel");
                slider.removeClass("owl-carousel");
                slider2.trigger("destroy.owl.carousel");
                slider2.removeClass("owl-carousel");
                itsSliderFlag = false;
            }
        }

        doIT();
        win.addEventListener("resize", function() {
            doIT();
        });
    }

    function autoHeight() {
        function doIt() {
            if (win.innerWidth > 479 && !autoHeightFlag) {
                $('.spares .spares-item').matchHeight({
                    byRow: true
                });
                $('.spares-item.spares-item--big  .spares-item__description').matchHeight({
                    byRow: false
                });
                $('.spares-item.spares-item--big2  .spares-item__description').matchHeight({
                    byRow: true
                });
                autoHeightFlag = true;
            }
        }

        doIt();
        win.addEventListener("resize", function() {
            doIt();
        });
    }

    function advCarousel() {
        var list = $(".advantages__list");
        var itsSliderFlag = false;

        function doIT() {
            if (win.innerWidth <= 1024 && !itsSliderFlag) {
                list.addClass("owl-carousel");
                itsSliderFlag = true;
                list.owlCarousel({
                    items: 4,
                    dots: true,
                    nav: true,
                    autoplay: false,
                    loop: true,
                    margin: 20,
                    responsive: {
                        0: {
                            items: 1,
                        },

                        570: {
                            items: 2,
                        },

                        850: {
                            items: 3,
                        }
                    }
                });
            }
            else if (win.innerWidth > 1024 && itsSliderFlag) {
                list.trigger("destroy.owl.carousel");
                list.removeClass("owl-carousel");
                itsSliderFlag = false;
            }
        }

        doIT();
        win.addEventListener("resize", function() {
            doIT();
        });
    }

    function adpImgVideoAdaptive() {
        var target = doc.querySelectorAll(".service-adp-block__img-video");
        var mob = doc.querySelectorAll(".service-adp-block__img-video-mobile");
        var desk = doc.querySelectorAll(".service-adp-block__img-video-desktop");
        var tlenght = target.length;
        var Flag = false;

        function doIT() {
            // if (win.innerWidth <= 1024 && !Flag){
            //     var par = target.parents(".service-adp-block");
            //     var mobileContainer = par.find(".service-adp-block__img-video-mobile");
            //     var desktopContainer = par.find(".service-adp-block__img-video-desktop");
            //     mobileContainer.appendChild(target);
            // }
            if (win.innerWidth <= 1024 && !Flag) {
                for (var i = 0; i < tlenght; i++) {
                    mob[i].appendChild(target[i]);
                }
                Flag = true;
            }
            else if (win.innerWidth > 1024 && Flag) {
                for (var i = 0; i < tlenght; i++) {
                    desk[i].appendChild(target[i]);
                }
                Flag = false;
            }

        }

        doIT();
        win.addEventListener("resize", function() {
            doIT();
        });
    }

    function headRoom() {
        var elem = doc.getElementById("page-header");
        var headerNavbar = doc.getElementById('header-navbar');
        var $headerNavbar =$('#header-navbar');
        var searchBar = doc.getElementById('search-bar');
        var headerLogo = doc.querySelector('.logo-pannel__logo');
        var phoneHeader = doc.querySelector('.logo-panel__phone');
        var logo = doc.querySelector('.logo-pannel__logo-pic');
        var logoPanel = doc.querySelector('.logo-panel');
        var duration = 200;
        var html = $('html');
        var flag = false;
        var init = true;
        var fl = false;
        var lastCoord = 99999999999;
        var headroom = new Headroom(elem, {
            "offset": 250,
            "tolerance": 5,
            "classes": {
                "initial": "animated",
                "pinned": "slideDown",
                "unpinned": "slideUp"
            },
            onPin: function() {
                Vel(headerNavbar, "slideDown", {duration: duration}, {easing: "none"});
                html.addClass("onPin");
                html.removeClass("onUnpin");
                html.removeClass("onTop");
                html.removeClass("onNotTop");
            },
            onUnpin: function() {
                Vel(headerNavbar, "slideUp", {duration: duration}, {easing: "none"}, {});
                flag = true;
                html.removeClass("onPin");
                html.addClass("onUnpin");
                html.removeClass("onTop");
                html.removeClass("onNotTop");
            },
            onTop: function() {
                Vel(logo, {height: "65px"}, {duration: duration}, {easing: "none"}, {complete: function() {console.log("complete");}});
                html.removeClass("onPin");
                html.removeClass("onUnpin");
                html.addClass("onTop");
                html.removeClass("onNotTop");
            },
            onNotTop: function() {
                html.removeClass("onPin");
                html.removeClass("onUnpin");
                html.removeClass("onTop");
                html.addClass("onNotTop");
                Vel(headerNavbar, "slideUp", {duration: duration}, {easing: "none"}, {});
                Vel(logo, {height: "50px"}, {duration: duration}, {easing: "none"}, {});
                logoPanel.classList.add("logo-panel--pinned");
                function de() {
                    var bodyScrollTop = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop;

                    if (bodyScrollTop > 300 && init === true) {
                        lastCoord = bodyScrollTop;
                        init = false;
                    }
                    if (bodyScrollTop < lastCoord && !fl) {
                        headroom.onPin();
                        fl = true;
                    }
                }

                de();
                win.addEventListener('scroll', function() {
                    de();
                })
            },
        });
        var stickyTarget = $("#article-anchor-nav-desk");

        headroom.init();
    }
    function anchorNav(){
        var $anchorNav = $("#article-anchor-nav");
    }
    win.addEventListener("load", function() {
        $('input[name="phone"]').mask('+7 (999) 999-99-99');
        carousels();
        mobileCardTitle();
        sandwitchDD();
        slideOut();
        anchorNav();
        advCarousel();
        searchBarF();
        dropdown();
        adaptiveSliderSpares();
        autoHeight();
    });
    doc.addEventListener("DOMContentLoaded", function() {
        adpImgVideoAdaptive();
        headRoom();
    });
    var lastTargetOffset = 0;

    $(function () {
        var winScrollLast = 0;
        $('.article__anchor-nav a[href*="#"]:not([href="#"])').click(function () {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                var html = $("html");
                var offset = 0;
                var speed = 600;
                target = target.length ? target : $('[id=' + this.hash.slice(1) + ']');
                if (target.length) {
                     console.log(winScrollLast + " winScrollLast");
                     console.log("______________________________________");
                     console.log(target.offset().top + " offsetTop");
                     console.log(lastTargetOffset + " lastTargetOffset");
                     if (win.innerWidth >= 1024) {
                         if (target.offset().top === lastTargetOffset && winScrollLast === $(win).scrollTop() &&  winScrollLast !== 0) {
                             return false
                         }
                         winScrollLast = $(win).scrollTop();
                         if (lastTargetOffset === 0 || (target.offset().top <= lastTargetOffset)) {
                             offset = 62;
                         }
                         lastTargetOffset = target.offset().top;
                         $('html, body').animate({
                             scrollTop: target.offset().top - offset
                         }, speed);
                         return false;
                     }
                    else if (win.innerWidth < 1024){
                         $('html, body').animate({
                             scrollTop: target.offset().top - $(".mobile-navbar").outerHeight()
                         }, 600);
                         return false;
                     }
                }
            }
        });
    });
})(window, document, window.jQuery);
