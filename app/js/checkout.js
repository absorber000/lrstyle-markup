;(function () {
    document.addEventListener("DOMContentLoaded", function() {

        // Open-close sections
        var editbtn1 = document.querySelector(".checkout__title-edit--text");
        var editbtn2 = document.querySelector(".checkout__title-edit--arrow");
        var section = document.querySelector(".checkout__section");
        if (!editbtn1 && !editbtn2) return;
        if (!section) return;

        editbtn1.addEventListener("click", function (event) {
            event.preventDefault();
            section.classList.toggle("checkout__section--close");
            section.classList.toggle("checkout__section--open");
        });
        editbtn2.addEventListener("click", function (event) {
            event.preventDefault();
            section.classList.toggle("checkout__section--close");
            section.classList.toggle("checkout__section--open");
        });

        // Section delivery
        var choose1      = document.querySelector(".checkout__choose-delivery-method--self");
        var choose2      = document.querySelector(".checkout__choose-delivery-method--courier");
        var choose2Input = document.querySelector("#deliveryMethodCourier");
        var choose3      = document.querySelector(".checkout__choose-delivery-method--transport");

        var subsection1 = document.querySelector(".checkout__self");
        var subsection2 = document.querySelector(".checkout__courier");
        var subsection3 = document.querySelector(".checkout__transport");

        if (!choose1 || !choose2 || !choose3 || !subsection1 || !subsection2 || !subsection3) return;
        if (!choose2Input) return;
        subsection1.classList.add("checkout__self--open");

        choose1.addEventListener("click", function (event) {
            subsection1.classList.add("checkout__self--open");
            subsection2.classList.remove("checkout__courier--open");
            subsection3.classList.remove("checkout__transport--open");

            // control border
            choose1.classList.add   ("checkout__choose-delivery-method--right-border-green");
            choose3.classList.remove("checkout__choose-delivery-method--left-border-green");
        });
        choose2.addEventListener("click", function (event) {
            subsection1.classList.remove("checkout__self--open");
            subsection2.classList.add("checkout__courier--open");
            subsection3.classList.remove("checkout__transport--open");

            // control border
            choose1.classList.add   ("checkout__choose-delivery-method--right-border-green");
            choose3.classList.add   ("checkout__choose-delivery-method--left-border-green");
        });
        choose3.addEventListener("click", function (event) {
            subsection1.classList.remove("checkout__self--open");
            subsection2.classList.remove("checkout__courier--open");
            subsection3.classList.add("checkout__transport--open");

            // control border
            choose1.classList.remove   ("checkout__choose-delivery-method--right-border-green");
            choose3.classList.add      ("checkout__choose-delivery-method--left-border-green");
        });

        // Add comment
        // Section delivery
        var addComment      = document.querySelector(".checkout__courier-addcomment");
        var CommentCourier = document.querySelector(".checkout__textarea--courier-comment");

        if (!addComment) return;
        if (!CommentCourier) return;

        addComment.addEventListener("click", function (event) {
            event.preventDefault();
            addComment.classList.add        ("checkout__courier-addcomment--hide");
            CommentCourier.classList.remove ("checkout__textarea--hide");
        });
    });
})();