;(function () {
    document.addEventListener("DOMContentLoaded", function() {

        // SELECT
        var formSelect = document.querySelector(".form__select--theme_grey");
        var formField  = document.querySelector(".form__select-field");
        var formItem  = document.querySelector(".form__select-item");
        var formList  = document.querySelector(".form__select-list");
        if (!formSelect) return;
        if (!formField) return;
        if (!formList) return;
        if (!formItem) return;
        formSelect.addEventListener("click", function (event) {
            event.preventDefault();
            formSelect.classList.toggle("form__select--open");
            event = event || window.event;
            var el = event.target || event.srcElement;
            if (formField === el) return;
            formField.value = el.innerHTML;
        });
        // formItem.addEventListener("click", function (event) {
        //     console.log(formField.value);
        //     event.preventDefault();
        //     formSelect.classList.toggle("form__select--open");
        //     event = event || window.event;
        //     var el = event.target || event.srcElement;
        //     formField.value = el.innerHTML;
        //
        // });
    });
})();