(function (factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery'], factory);
    } else if (typeof exports === 'object') {
        // Node/CommonJS
        factory(require('jquery'));
    } else {
        // Browser globals
        factory(jQuery);
    }
}(function ($) {

    var ua = navigator.userAgent,
        iPhone = /iphone/i.test(ua),
        chrome = /chrome/i.test(ua),
        android = /android/i.test(ua),
        caretTimeoutId;

    $.mask = {
        //Predefined character definitions
        definitions: {
            '9': "[0-9]",
            'a': "[A-Za-z]",
            '*': "[A-Za-z0-9]"
        },
        autoclear: true,
        dataName: "rawMaskFn",
        placeholder: '_'
    };

    $.fn.extend({
        //Helper Function for Caret positioning
        caret: function (begin, end) {
            var range;

            if (this.length === 0 || this.is(":hidden")) {
                return;
            }

            if (typeof begin == 'number') {
                end = (typeof end === 'number') ? end : begin;
                return this.each(function () {
                    if (this.setSelectionRange) {
                        this.setSelectionRange(begin, end);
                    } else if (this.createTextRange) {
                        range = this.createTextRange();
                        range.collapse(true);
                        range.moveEnd('character', end);
                        range.moveStart('character', begin);
                        range.select();
                    }
                });
            } else {
                if (this[0].setSelectionRange) {
                    begin = this[0].selectionStart;
                    end = this[0].selectionEnd;
                } else if (document.selection && document.selection.createRange) {
                    range = document.selection.createRange();
                    begin = 0 - range.duplicate().moveStart('character', -100000);
                    end = begin + range.text.length;
                }
                return {begin: begin, end: end};
            }
        },
        unmask: function () {
            return this.trigger("unmask");
        },
        mask: function (mask, settings) {
            var input,
                defs,
                tests,
                partialPosition,
                firstNonMaskPos,
                lastRequiredNonMaskPos,
                len,
                oldVal;

            if (!mask && this.length > 0) {
                input = $(this[0]);
                var fn = input.data($.mask.dataName)
                return fn ? fn() : undefined;
            }

            settings = $.extend({
                autoclear: $.mask.autoclear,
                placeholder: $.mask.placeholder, // Load default placeholder
                completed: null
            }, settings);


            defs = $.mask.definitions;
            tests = [];
            partialPosition = len = mask.length;
            firstNonMaskPos = null;

            $.each(mask.split(""), function (i, c) {
                if (c == '?') {
                    len--;
                    partialPosition = i;
                } else if (defs[c]) {
                    tests.push(new RegExp(defs[c]));
                    if (firstNonMaskPos === null) {
                        firstNonMaskPos = tests.length - 1;
                    }
                    if (i < partialPosition) {
                        lastRequiredNonMaskPos = tests.length - 1;
                    }
                } else {
                    tests.push(null);
                }
            });

            return this.trigger("unmask").each(function () {
                var input = $(this),
                    buffer = $.map(
                        mask.split(""),
                        function (c, i) {
                            if (c != '?') {
                                return defs[c] ? getPlaceholder(i) : c;
                            }
                        }),
                    defaultBuffer = buffer.join(''),
                    focusText = input.val();

                function tryFireCompleted() {
                    if (!settings.completed) {
                        return;
                    }

                    for (var i = firstNonMaskPos; i <= lastRequiredNonMaskPos; i++) {
                        if (tests[i] && buffer[i] === getPlaceholder(i)) {
                            return;
                        }
                    }
                    settings.completed.call(input);
                }

                function getPlaceholder(i) {
                    if (i < settings.placeholder.length)
                        return settings.placeholder.charAt(i);
                    return settings.placeholder.charAt(0);
                }

                function seekNext(pos) {
                    while (++pos < len && !tests[pos]);
                    return pos;
                }

                function seekPrev(pos) {
                    while (--pos >= 0 && !tests[pos]);
                    return pos;
                }

                function shiftL(begin, end) {
                    var i,
                        j;

                    if (begin < 0) {
                        return;
                    }

                    for (i = begin, j = seekNext(end); i < len; i++) {
                        if (tests[i]) {
                            if (j < len && tests[i].test(buffer[j])) {
                                buffer[i] = buffer[j];
                                buffer[j] = getPlaceholder(j);
                            } else {
                                break;
                            }

                            j = seekNext(j);
                        }
                    }
                    writeBuffer();
                    input.caret(Math.max(firstNonMaskPos, begin));
                }

                function shiftR(pos) {
                    var i,
                        c,
                        j,
                        t;

                    for (i = pos, c = getPlaceholder(pos); i < len; i++) {
                        if (tests[i]) {
                            j = seekNext(i);
                            t = buffer[i];
                            buffer[i] = c;
                            if (j < len && tests[j].test(t)) {
                                c = t;
                            } else {
                                break;
                            }
                        }
                    }
                }

                function androidInputEvent(e) {
                    console.log(input);
                    var curVal = input.val();
                    var pos = input.caret();
                    if (oldVal && oldVal.length && oldVal.length > curVal.length) {
                        // a deletion or backspace happened
                        checkVal(true);
                        while (pos.begin > 0 && !tests[pos.begin - 1])
                            pos.begin--;
                        if (pos.begin === 0) {
                            while (pos.begin < firstNonMaskPos && !tests[pos.begin])
                                pos.begin++;
                        }
                        input.caret(pos.begin, pos.begin);
                    } else {
                        var pos2 = checkVal(true);
                        var lastEnteredValue = curVal.charAt(pos.begin);
                        if (pos.begin < len) {
                            if (!tests[pos.begin]) {
                                pos.begin++;
                                if (tests[pos.begin].test(lastEnteredValue)) {
                                    pos.begin++;
                                }
                            } else {
                                if (tests[pos.begin].test(lastEnteredValue)) {
                                    pos.begin++;
                                }
                            }
                        }
                        input.caret(pos.begin, pos.begin);
                    }
                    tryFireCompleted();
                }


                function blurEvent(e) {
                    checkVal();

                    if (input.val() != focusText)
                        input.change();
                }

                function keydownEvent(e) {
                    if (input.prop("readonly")) {
                        return;
                    }

                    var k = e.which || e.keyCode,
                        pos,
                        begin,
                        end;
                    oldVal = input.val();
                    //backspace, delete, and escape get special treatment
                    if (k === 8 || k === 46 || (iPhone && k === 127)) {
                        pos = input.caret();
                        begin = pos.begin;
                        end = pos.end;

                        if (end - begin === 0) {
                            begin = k !== 46 ? seekPrev(begin) : (end = seekNext(begin - 1));
                            end = k === 46 ? seekNext(end) : end;
                        }
                        clearBuffer(begin, end);
                        shiftL(begin, end - 1);

                        e.preventDefault();
                    } else if (k === 13) { // enter
                        blurEvent.call(this, e);
                    } else if (k === 27) { // escape
                        input.val(focusText);
                        input.caret(0, checkVal());
                        e.preventDefault();
                    }
                }

                function keypressEvent(e) {
                    if (input.prop("readonly")) {
                        return;
                    }

                    var k = e.which || e.keyCode,
                        pos = input.caret(),
                        p,
                        c,
                        next;

                    if (e.ctrlKey || e.altKey || e.metaKey || k < 32) {//Ignore
                        return;
                    } else if (k && k !== 13) {
                        if (pos.end - pos.begin !== 0) {
                            clearBuffer(pos.begin, pos.end);
                            shiftL(pos.begin, pos.end - 1);
                        }

                        p = seekNext(pos.begin - 1);
                        if (p < len) {
                            c = String.fromCharCode(k);
                            if (tests[p].test(c)) {
                                shiftR(p);

                                buffer[p] = c;
                                writeBuffer();
                                next = seekNext(p);

                                if (android) {
                                    //Path for CSP Violation on FireFox OS 1.1
                                    var proxy = function () {
                                        $.proxy($.fn.caret, input, next)();
                                    };

                                    setTimeout(proxy, 0);
                                } else {
                                    input.caret(next);
                                }
                                if (pos.begin <= lastRequiredNonMaskPos) {
                                    tryFireCompleted();
                                }
                            }
                        }
                        e.preventDefault();
                    }
                }

                function clearBuffer(start, end) {
                    var i;
                    for (i = start; i < end && i < len; i++) {
                        if (tests[i]) {
                            buffer[i] = getPlaceholder(i);
                        }
                    }
                }

                function writeBuffer() {
                    input.val(buffer.join(''));
                }

                function checkVal(allow) {
                    //try to place characters where they belong
                    var test = input.val(),
                        lastMatch = -1,
                        i,
                        c,
                        pos;

                    for (i = 0, pos = 0; i < len; i++) {
                        if (tests[i]) {
                            buffer[i] = getPlaceholder(i);
                            while (pos++ < test.length) {
                                c = test.charAt(pos - 1);
                                if (tests[i].test(c)) {
                                    buffer[i] = c;
                                    lastMatch = i;
                                    break;
                                }
                            }
                            if (pos > test.length) {
                                clearBuffer(i + 1, len);
                                break;
                            }
                        } else {
                            if (buffer[i] === test.charAt(pos)) {
                                pos++;
                            }
                            if (i < partialPosition) {
                                lastMatch = i;
                            }
                        }
                    }
                    if (allow) {
                        writeBuffer();
                    } else if (lastMatch + 1 < partialPosition) {
                        if (settings.autoclear || buffer.join('') === defaultBuffer) {
                            // Invalid value. Remove it and replace it with the
                            // mask, which is the default behavior.
                            if (input.val()) input.val("");
                            clearBuffer(0, len);
                        } else {
                            // Invalid value, but we opt to show the value to the
                            // user and allow them to correct their mistake.
                            writeBuffer();
                        }
                    } else {
                        writeBuffer();
                        input.val(input.val().substring(0, lastMatch + 1));
                    }
                    return (partialPosition ? i : firstNonMaskPos);
                }

                input.data($.mask.dataName, function () {
                    return $.map(buffer, function (c, i) {
                        return tests[i] && c != getPlaceholder(i) ? c : null;
                    }).join('');
                });


                input
                    .one("unmask", function () {
                        input
                            .off(".mask")
                            .removeData($.mask.dataName);
                    })
                    .on("focus.mask", function () {
                        if (input.prop("readonly")) {
                            return;
                        }

                        clearTimeout(caretTimeoutId);
                        var pos;

                        focusText = input.val();

                        pos = checkVal();

                        caretTimeoutId = setTimeout(function () {
                            if (input.get(0) !== document.activeElement) {
                                return;
                            }
                            writeBuffer();
                            if (pos == mask.replace("?", "").length) {
                                input.caret(0, pos);
                            } else {
                                input.caret(pos);
                            }
                        }, 10);
                    })
                    .on("blur.mask", blurEvent)
                    .on("keydown.mask", keydownEvent)
                    .on("keypress.mask", keypressEvent)
                    .on("input.mask paste.mask", function () {
                        if (input.prop("readonly")) {
                            return;
                        }

                        setTimeout(function () {
                            var pos = checkVal(true);
                            input.caret(pos);
                            tryFireCompleted();
                        }, 0);
                    });
                if (chrome && android) {
                    input
                        .off('input.mask')
                        .on('input.mask', androidInputEvent);
                }
                checkVal(); //Perform initial check for existing values
            });
        }
    });
}));
'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

;(function (win, doc) {
    'use strict';

    if (window.jQuery) {
        var Vel = $.Velocity;
    } else {
        var Vel = Velocity;
    }

    var Custom = function () {
        function Custom(name) {
            _classCallCheck(this, Custom);

            this.name = name;
        }

        _createClass(Custom, [{
            key: 'wrapElement',
            value: function wrapElement(element, wrapper) {
                wrapper = wrapper || doc.createElement('div');
                if (element.nextSibling) {
                    element.parentNode.insertBefore(wrapper, element.nextSibling);
                } else {
                    element.parentNode.appendChild(wrapper);
                }
                return wrapper.appendChild(element);
            }
        }, {
            key: 'insertAfter',
            value: function insertAfter(elem, refElem) {
                var parent = refElem.parentNode;
                var next = refElem.nextSibling;
                if (next) {
                    return parent.insertBefore(elem, next);
                } else {
                    return parent.appendChild(elem);
                }
            }
        }, {
            key: 'getSiblings',
            value: function getSiblings(elem) {
                var siblings = [];
                var sibling = elem.parentNode.firstChild;
                for (; sibling; sibling = sibling.nextSibling) {
                    if (sibling.nodeType === 1 && sibling !== elem) {
                        siblings.push(sibling);
                    }
                }
                return siblings;
            }
        }]);

        return Custom;
    }();

    var CSelect = function (_Custom) {
        _inherits(CSelect, _Custom);

        function CSelect(el, details, setActiveItemCallback) {
            _classCallCheck(this, CSelect);

            var _this = _possibleConstructorReturn(this, (CSelect.__proto__ || Object.getPrototypeOf(CSelect)).call(this));

            _this.store = {};
            _this.details = details;
            _this.store.select = el;
            _this.details.setActiveItemCallback = setActiveItemCallback;
            _this.init();
            _this.addEventListeners();
            _this.par = details.par;
            return _this;
        }

        _createClass(CSelect, [{
            key: 'getLabel',
            value: function getLabel(value) {
                var res = void 0;
                for (var i = 0; i < this.store.choicesLength; i++) {
                    var el = this.details.choices[i];
                    if (el.value === value) {
                        res = el.label;
                        this.store.activeEl = this.store.arrElms[i];
                        this.store.arrElms[i].classList.add("selectize__item--active");
                        break;
                    }
                }
                return res;
            }
        }, {
            key: 'createSelectizeList',
            value: function createSelectizeList(arr, dest) {
                var arrLength = arr.length;
                this.store.arrElms = [];
                for (var i = 0; i < arrLength; i++) {
                    var el = arr[i];
                    var listEl = doc.createElement('div');
                    listEl.classList.add("selectize__item");
                    this.store.arrElms.push(listEl);
                    listEl.innerHTML = el.label;
                    listEl.setAttribute('data-selectize-item', i);
                    dest.appendChild(listEl);
                }
            }
        }, {
            key: 'setActiveItem',
            value: function setActiveItem(el) {
                var prevActiveEl = this.store.activeEl;
                var thisAttr = el.getAttribute('data-selectize-item');
                this.store.activeEl = el;
                el.classList.add("selectize__item--active");
                prevActiveEl.classList.remove("selectize__item--active");
                this.store.singleOption.innerHTML = this.details.choices[thisAttr].label;
                this.store.singleOption.value = this.details.choices[thisAttr].value;
                this.store.single.innerHTML = this.details.choices[thisAttr].label;
                this.dropdownClose();
                if (this.details.setActiveItemCallback) {
                    this.details.setActiveItemCallback();
                }
            }
        }, {
            key: 'renderChoices',
            value: function renderChoices(arr) {
                var listArr = this.store.list.childNodes;
                var prevArrLength = listArr.length;
                var thisArrLength = arr.length;
                this.details.choices = arr;
                for (var j = 0; j < prevArrLength; j++) {
                    var el = listArr[j];
                    this.store.arrElms.shift();
                }
                this.store.list.parentNode.removeChild(this.store.list);
                this.store.list = doc.createElement('div');
                this.store.list.classList.add("selectize__list");
                this.store.list.classList.add("selectize__list--dropdown");
                this.store.selectize.appendChild(this.store.list);
                for (var i = 0; i < thisArrLength; i++) {
                    var _el = arr[i];
                    var listEl = doc.createElement('div');
                    listEl.classList.add("selectize__item");
                    //this.store.arrElms.shift();
                    this.store.arrElms.unshift(listEl);
                    listEl.innerHTML = _el.label;
                    listEl.setAttribute('data-selectize-item', i);
                    this.store.list.appendChild(listEl);
                    if (i === 0) {
                        listEl.classList.add("selectize__item--active");
                        this.store.activeEl.classList.remove("selectize__item--active");
                        this.store.activeEl = listEl;
                    }
                }
                this.store.single.innerHTML = arr[0].label;
                this.store.singleOption.innerHTML = arr[0].label;
                this.store.singleOption.value = arr[0].value;
            }
        }, {
            key: 'dropdownOpen',
            value: function dropdownOpen() {
                this.store.list.classList.add("is-active");
                this.store.hasActiveDropdown = true;
            }
        }, {
            key: 'dropdownClose',
            value: function dropdownClose() {
                this.store.list.classList.remove("is-active");
                this.store.hasActiveDropdown = false;
            }
        }, {
            key: '_onClick',
            value: function _onClick(e) {
                var target = e.target;

                if (this.store.selectize.contains(target)) {
                    console.log(this.store.hasActiveDropdown);
                    console.log(typeof target === 'undefined' ? 'undefined' : _typeof(target));
                    if (target.classList.contains("selectize__list--single") && !this.store.hasActiveDropdown) {
                        // открытие дропдауна
                        this.dropdownOpen();
                    } else if (target.classList.contains("selectize__list--single") && this.store.hasActiveDropdown) {
                        this.dropdownClose();
                    }
                    if (target.classList.contains("selectize__item") && !target.classList.contains("selectize__item--active")) {
                        target.classList.add("selectize__item--active");
                        this.setActiveItem(target);
                    } else if (target.classList.contains("selectize__item") && target.classList.contains("selectize__item--active")) {
                        this.dropdownClose();
                    }
                } else {
                    if (this.store.hasActiveDropdown) {
                        this.dropdownClose();
                    }
                }
            }
        }, {
            key: 'addEventListeners',
            value: function addEventListeners() {
                var _this2 = this;

                doc.addEventListener('click', function (event) {
                    return _this2._onClick(event);
                });
                // doc.addEventListener('click', event => this.onMouseDown(event));
            }
        }, {
            key: 'init',
            value: function init() {
                this.store.single = doc.createElement('div');
                this.store.inner = doc.createElement('div');
                this.store.selectize = doc.createElement('div');
                this.store.list = doc.createElement('div');

                //__________________________________________________________-
                this.store.hasActiveDropdown = false;
                //__________________________________________________________-

                // let selectOptionArray = this.el.querySelectorAll("option");
                this.createSelectizeList(this.details.choices, this.store.list);
                this.store.select.classList.add("is-hidden");
                this.store.choicesLength = this.details.choices.length;

                this.store.single.innerHTML = this.details.choices[0].label;
                this.store.singleOption = doc.createElement('option');
                this.store.singleOption.innerHTML = this.details.choices[0].label;
                this.store.singleOption.value = this.details.selected;
                this.store.select.appendChild(this.store.singleOption);

                this.store.selectize.classList.add("selectize");
                if (this.details.subclass) {
                    this.store.selectize.classList.add(this.details.subclass);
                }
                this.store.list.classList.add("selectize__list");
                this.store.list.classList.add("selectize__list--dropdown");
                this.store.single.classList.add("selectize__list");
                this.store.single.classList.add("selectize__list--single");
                this.store.inner.classList.add("selectize__inner");
                _get(CSelect.prototype.__proto__ || Object.getPrototypeOf(CSelect.prototype), 'wrapElement', this).call(this, this.store.select, this.store.inner);
                _get(CSelect.prototype.__proto__ || Object.getPrototypeOf(CSelect.prototype), 'wrapElement', this).call(this, this.store.inner, this.store.selectize);
                _get(CSelect.prototype.__proto__ || Object.getPrototypeOf(CSelect.prototype), 'insertAfter', this).call(this, this.store.list, this.store.inner);
                _get(CSelect.prototype.__proto__ || Object.getPrototypeOf(CSelect.prototype), 'insertAfter', this).call(this, this.store.single, this.store.select);
            }
        }]);

        return CSelect;
    }(Custom);

    var CAccordeon = function (_Custom2) {
        _inherits(CAccordeon, _Custom2);

        function CAccordeon(el, details) {
            _classCallCheck(this, CAccordeon);

            var _this3 = _possibleConstructorReturn(this, (CAccordeon.__proto__ || Object.getPrototypeOf(CAccordeon)).call(this));

            _this3.store = {};
            _this3.details = details;
            _this3.store.acc = el;
            _this3.init();
            _this3.addEventListeners();
            return _this3;
        }

        _createClass(CAccordeon, [{
            key: 'toggle',
            value: function toggle(el) {
                var attrTitle = el.getAttribute('data-acc-title');
                var thisBlock = this.store.blocks[attrTitle];
                var content = this.store.arrWrappers[attrTitle];
                if (this.details.multiple) {
                    if (!el.classList.contains(this.details.clsactive)) {
                        el.classList.add(this.details.clsactive);
                        thisBlock.classList.add(this.details.clsactive);
                        Vel(content, "slideDown", { duration: 300 }, { easing: "none" }, {
                            complete: function complete() {
                                content.classList.add("is-hidden");
                                el.classList.remove(this.details.clsactive);
                            }
                        }, {
                            begin: function begin() {
                                content.classList.remove("is-hidden");
                            }
                        });
                    } else if (el.classList.contains(this.details.clsactive)) {
                        el.classList.remove(this.details.clsactive);
                        thisBlock.classList.remove(this.details.clsactive);
                        Vel(content, "slideUp", { duration: 300 }, { easing: "none" }, { complete: function complete() {
                                console.log("complete");
                            } });
                    }
                }
            }
        }, {
            key: '_onClick',
            value: function _onClick(e) {
                var target = e.target;
                if (this.store.acc.contains(target)) {
                    if (win.innerWidth > 1024 && this.details.flag) {
                        return;
                    }
                    while (target.classList !== this.details.toggle) {
                        if (target.classList.contains(this.details.toggle)) {
                            this.toggle(target);
                            e.preventDefault();
                            return;
                        }
                        target = target.parentNode;
                    }
                }
            }
        }, {
            key: 'addEventListeners',
            value: function addEventListeners() {
                var _this4 = this;

                doc.addEventListener('click', function (event) {
                    return _this4._onClick(event);
                });
            }
        }, {
            key: 'destroy',
            value: function destroy() {
                for (var i = 0; i < this.store.arrWrappers.length; i++) {
                    var el = this.store.arrWrappers[i];
                    var elT = this.store.arrTitles[i];
                    el.classList.remove('is-hidden');
                    elT.classList.remove(this.details.clsactive);
                    el.style.display = "block";
                }
            }
        }, {
            key: 'finit',
            value: function finit() {
                for (var i = 0; i < this.store.arrWrappers.length; i++) {
                    var el = this.store.arrWrappers[i];
                    el.classList.add('is-hidden');
                    el.style.display = "none";
                }
            }
        }, {
            key: 'init',
            value: function init() {
                this.store.arrTitles = [];
                this.store.arrContainers = [];
                this.store.blocks = [];
                this.store.arrWrappers = [];
                var titles = this.store.acc.querySelectorAll("." + this.details.toggle);
                var containers = this.store.acc.querySelectorAll("." + this.details.containers);
                var blocks = this.store.acc.querySelectorAll("." + this.details.blocks);
                var titlesLength = titles.length;
                for (var i = 0; i < titlesLength; i++) {
                    var titlesEl = titles[i];
                    var containersEl = containers[i];
                    var blockEl = blocks[i];
                    blockEl.setAttribute('data-acc-block', i);
                    var wrapper = doc.createElement('div');
                    this.store.arrTitles.push(titlesEl);
                    this.store.arrContainers.push(containersEl);
                    this.store.arrWrappers.push(wrapper);
                    this.store.blocks.push(blockEl);
                    titlesEl.setAttribute('data-acc-title', i);
                    containersEl.setAttribute('data-acc-container', i);

                    wrapper.setAttribute('data-acc-wrapper', i);
                    wrapper.classList.add("acc-wrapper", "is-hidden");
                    _get(CAccordeon.prototype.__proto__ || Object.getPrototypeOf(CAccordeon.prototype), 'wrapElement', this).call(this, containersEl, wrapper);
                }
            }
        }]);

        return CAccordeon;
    }(Custom);

    var CHeader = function (_Custom3) {
        _inherits(CHeader, _Custom3);

        function CHeader(el, details) {
            _classCallCheck(this, CHeader);

            var _this5 = _possibleConstructorReturn(this, (CHeader.__proto__ || Object.getPrototypeOf(CHeader)).call(this));

            _this5.details = details;
            _this5.details.header = el;
            _this5.init();
            _this5.addEventListeners();
            return _this5;
        }

        _createClass(CHeader, [{
            key: 'setMatchHeight',
            value: function setMatchHeight() {
                this.details.sitewrapper.style.paddingTop = this.details.header.clientHeight + "px";
            }
        }, {
            key: 'init',
            value: function init() {
                this.setMatchHeight();
            }
        }, {
            key: '_onResize',
            value: function _onResize() {
                this.setMatchHeight();
            }
        }, {
            key: 'addEventListeners',
            value: function addEventListeners() {
                var _this6 = this;

                win.addEventListener('resize', function (event) {
                    return _this6._onResize();
                });
            }
        }]);

        return CHeader;
    }(Custom);

    var CDropdown = function (_Custom4) {
        _inherits(CDropdown, _Custom4);

        function CDropdown(e) {
            _classCallCheck(this, CDropdown);

            // this.details = details;
            // this.details.header = el;
            var _this7 = _possibleConstructorReturn(this, (CDropdown.__proto__ || Object.getPrototypeOf(CDropdown)).call(this));

            _this7.init();
            _this7.addEventListeners();
            return _this7;
        }

        _createClass(CDropdown, [{
            key: '_onClick',
            value: function _onClick(e) {}
        }, {
            key: 'addEventListeners',
            value: function addEventListeners() {
                var _this8 = this;

                doc.addEventListener('click', function (event) {
                    return _this8._onClick();
                });
            }
        }, {
            key: 'init',
            value: function init() {
                var el = doc.querySelectorAll('[data-dropdown]')[0];
            }
        }]);

        return CDropdown;
    }(Custom);

    var CModelChecker = function (_Custom5) {
        _inherits(CModelChecker, _Custom5);

        function CModelChecker(list, selEngine, selYear, models, clickF) {
            _classCallCheck(this, CModelChecker);

            var _this9 = _possibleConstructorReturn(this, (CModelChecker.__proto__ || Object.getPrototypeOf(CModelChecker)).call(this));

            _this9.list = list;
            _this9.selEngine = selEngine;
            _this9.selYear = selYear;
            _this9.models = models;
            _this9.clickF = clickF;
            _this9.init();
            _this9.addEventListeners();
            return _this9;
        }

        _createClass(CModelChecker, [{
            key: '_onClick',
            value: function _onClick(e) {
                var target = e.target;
                if (this.list.contains(target) && target.classList.contains("tech-serv__model-checker-item")) {
                    this.clickF(e);
                    if (target !== this.activeEl) {
                        target.classList.add("active");
                        this.activeEl.classList.remove("active");
                        this.activeEl = target;
                    }
                }
            }
        }, {
            key: 'addEventListeners',
            value: function addEventListeners() {
                var _this10 = this;

                doc.addEventListener('click', function (event) {
                    return _this10._onClick(event);
                });
            }
        }, {
            key: 'init',
            value: function init() {
                var counter = 0;
                var row = void 0;
                for (var key in this.models) {
                    var item = doc.createElement('div');
                    item.classList.add("tech-serv__model-checker-item");
                    // item.setAttribute('data-value', this.models)
                    item.innerHTML = this.models[counter].label;
                    item.setAttribute('data-checker-id', counter);
                    if (counter === 0) {
                        item.classList.add("active");
                        this.activeEl = item;
                    }
                    if (counter % 3 === 0) {
                        row = doc.createElement('div');
                        row.classList.add("tech-serv__model-checker-row");
                        this.list.appendChild(row);
                    }
                    row.appendChild(item);
                    counter++;
                }
            }
        }]);

        return CModelChecker;
    }(Custom);

    function modelsAcc() {
        var acc = doc.getElementById("models-acc");
        var modelsAcc;
        var flag = false;
        var first = false;
        if (!acc) {
            return;
        }

        function doit() {
            if (win.innerWidth <= 1024 && !flag && !first) {
                modelsAcc = new CAccordeon(acc, {
                    showfirst: false,
                    multiple: true,
                    animate: false,
                    duration: 300,
                    toggle: "page-index__models-acc-title",
                    containers: "page-index__models-acc-content",
                    blocks: "page-index__models-acc-item",
                    flag: true,
                    clsactive: "is-active"
                });
                first = true;
                flag = true;
            }
            if (win.innerWidth <= 1024 && !flag) {
                modelsAcc.finit();
                flag = true;
            } else if (win.innerWidth > 1024 && flag) {
                modelsAcc.destroy();
                flag = false;
            }
        }

        doit();
        win.addEventListener('resize', doit);
    }

    function ArrayFromModels(obj) {
        var arr = [];
        for (var key in obj) {
            arr.push(obj[key]);
            var engines = [];
            for (var key1 in obj[key].engines) {
                engines.push(obj[key].engines[key1]);
                var years = [];
                for (var key2 in obj[key].engines[key1].years) {
                    years.push(obj[key].engines[key1].years[key2]);
                }
                obj[key].engines[key1].years = years;
            }
            obj[key].engines = engines;
        }
        return arr;
    }

    doc.addEventListener("DOMContentLoaded", function () {
        var carEngine = doc.getElementById("car-engine");
        var carYear = doc.getElementById("car-year");
        var acc = doc.getElementById("to-accordion");
        var header = doc.getElementById("page-header");
        var siteWrapper = doc.getElementById("site-wrapper");
        var modelsInput = doc.querySelector('.tech-serv__models-input');
        var stickyEl = $("#sidebar-sticky-part");
        stickyEl.stick_in_parent().on("sticky_kit:stick", function (e) {
            stickyEl.addClass("stick");
        }).on("sticky_kit:bottom", function (e) {
            stickyEl.addClass("unstick");
        }).on("sticky_kit:unbottom", function (e) {
            stickyEl.removeClass("unstick");
        });
        if (models) {
            models = ArrayFromModels(models);
            if (modelChecker) {
                var modelCheckerObj = new CModelChecker(modelChecker, carEngine, carYear, models, function (e) {
                    var target = e.target;
                    var id = target.getAttribute('data-checker-id');
                    selEngines.dropdownClose();
                    selYears.dropdownClose();
                    selEngines.renderChoices(models[id].engines);
                    selYears.renderChoices(models[id].engines[0].years);
                    modelsInput.setAttribute("value", models[id].value);
                });
                modelsInput.setAttribute("value", models[0].value);
            }
        }

        if (carYear) {
            var selYears = new CSelect(carYear, {
                choices: models[0].engines[0].years,
                selected: "0",
                subclass: "selectize--dark"
            });
        }

        if (carEngine) {
            var selEngines = new CSelect(carEngine, {
                choices: models[0].engines,
                selected: "1",
                subclass: "selectize--dark"
            }, function () {
                var id = selEngines.store.activeEl.getAttribute("data-selectize-item");
                var tabId = modelCheckerObj.activeEl.getAttribute("data-checker-id");
                selYears.renderChoices(models[tabId].engines[id].years);
            });
        }
        if (acc) {
            var toAcc = new CAccordeon(acc, {
                showfirst: false,
                multiple: true,
                animate: false,
                duration: 300,
                toggle: "to-accordion__title",
                containers: "to-accordion__content",
                blocks: "to-accordion__block",
                clsactive: "is-active"
            });
        }
        modelsAcc();
        var Header = new CHeader(header, {
            sitewrapper: siteWrapper
        });
        var dd = new CDropdown();

        var headerNavbar = doc.getElementById("header-navbar");
    });
})(window, document);

;(function (win, doc, $) {
    'use strict';

    if (window.jQuery) {
        var Vel = $.Velocity;
    } else {
        var Vel = Velocity;
    }

    var body = doc.querySelectorAll("body")[0];
    var html = doc.querySelectorAll("html")[0];
    var header = doc.querySelector("#page-header");
    var logoPanel = $(".logo-panel");
    var searchBar = $("#search-bar");
    var navbar = $(".page-header__navbar");
    var headerLogo = $(".logo-pannel__logo img");
    var overlay = $('#site-wrapper-overlay');
    var overlay2 = $('#site-wrapper-overlay2');
    var headerOverlay = $("#page-header-overlay");
    var searchBarInput = $(".searchbar__input input");
    var mobileNavbar = $(".mobile-navbar");
    var navbarPinnedFlag = false;
    var navbarPinnedFlagFirst = false;
    var navPinnedFlag = false;
    var autoHeightFlag = false;
    var siteWrapper = $(".site-wrapper");
    var Header = $(".page-header");
    var b = $('body');
    var bWidth = b.outerWidth();
    var scrollbar = window.innerWidth - bWidth;

    function overlayIt() {
        overlay.stop().fadeIn(200, function () {
            overlay.addClass("open");
        });
    }

    function overlayOver() {
        overlay.stop().fadeOut(200, function () {
            overlay.removeClass("open");
        });
    }

    function headerOverlayIt() {
        headerOverlay.stop().fadeIn(150, function () {
            headerOverlay.addClass("open");
        });
    }

    function headerOverlayOver() {
        headerOverlay.stop().fadeOut(150, function () {
            headerOverlay.removeClass("open");
        });
    }

    // $(".page-header__number-toggle").trigger('openIt');
    function slideOut() {
        var slideout = new Slideout({
            'panel': document.getElementById('site-wrapper'),
            'menu': document.getElementById('slide-menu'),
            'padding': 268,
            'tolerance': 70
        });
        var toggle = document.getElementById('js-mobile-menu-toggle');
        slideout.disableTouch();
        var offcanvas = document.getElementById('offcanvas');
        var wrapper = document.getElementById('site-wrapper');
        var btnFilter = document.querySelectorAll(".btn-filter")[0];
        var mobileFilter = document.getElementById('mobile-filter');
        var overlay = document.getElementById('site-wrapper-overlay');
        var slideMenuClose = $(".slide-menu__close");
        var b = $('body');
        var open = false;
        slideMenuClose.css({ "left": "-310px" });
        var jumpFix = function jumpFix() {
            var bWidth = b.outerWidth();
            var scrollbar = window.innerWidth - bWidth;

            slideout.on('beforeopen', function () {
                overlayIt();

                if (window.innerWidth - bWidth != 0) {
                    b.css({ "padding-right": scrollbar });
                }
            });

            slideout.on('close', function () {
                overlayOver();
                b.css({ "padding-right": "0" });
            });
        };

        function toggleClose() {
            if (open) {
                slideMenuClose.velocity({
                    properties: { left: "-310px" },
                    options: { duration: 250 }
                });
                open = false;
            } else {
                slideMenuClose.velocity({
                    properties: { left: "268px" },
                    options: { duration: 220 }
                });
                open = true;
            }
        }

        slideMenuClose.click(function () {
            toggleClose();
            slideout.toggle();
        });

        toggle.addEventListener('click', function () {
            toggleClose();
            slideout.toggle();
        });

        jumpFix();

        win.addEventListener("resize", function () {
            jumpFix();
        });
    }

    function searchBarF() {
        var searchBarCloseEl = $(".searchbar__close");
        var searchBarBtn = $(".logo-panel__searchBtn");
        var mobileSearchTrigger = $(".mobile-navbar__search");

        function searchBarOpen() {
            if (win.innerWidth <= 1024) {
                searchBar.stop().slideDown(150, function () {
                    searchBar.addClass("searchbar--visible");
                });
            } else {
                searchBar.stop().fadeIn(150, function () {
                    searchBar.addClass("searchbar--visible");
                });
            }
        }

        function searchBarClose() {
            if (win.innerWidth <= 1024) {
                searchBar.stop().slideUp(150, function () {
                    searchBar.removeClass("searchbar--visible");
                });
            } else {
                searchBar.stop().fadeOut(150, function () {
                    searchBar.removeClass("searchbar--visible");
                });
            }
        }

        mobileSearchTrigger.click(function () {
            if (!mobileSearchTrigger.hasClass("active")) {
                searchBarOpen();
                overlay2.stop().fadeIn(150, function () {
                    overlay2.addClass("open");
                });
                mobileNavbar.addClass("searchbar-open");
                mobileSearchTrigger.addClass("active");
            } else if (mobileSearchTrigger.hasClass("active")) {
                searchBarClose();
                overlay2.stop().fadeOut(150, function () {
                    overlay2.removeClass("open");
                });
                mobileNavbar.removeClass("searchbar-open");
                mobileSearchTrigger.removeClass("active");
            }
        });
        searchBarCloseEl.click(function () {
            headerOverlayOver();
            overlayOver();
            overlay2.stop().fadeOut(150, function () {
                overlay2.removeClass("open");
            });
            searchBarClose();
            mobileSearchTrigger.removeClass("active");
        });
        overlay2.click(function () {
            overlay2.stop().fadeOut(150, function () {
                overlay2.removeClass("open");
            });
            headerOverlayOver();
            searchBarClose();
            mobileSearchTrigger.removeClass("active");
        });
        searchBarBtn.click(function () {
            searchBarOpen();
            headerOverlayIt();
            overlayIt();
            headerOverlay.click(function () {
                headerOverlayOver();
                overlayOver();
                searchBarClose();
            });
            overlay.click(function () {
                overlayOver();
                headerOverlayOver();
                searchBarClose();
            });
        });
    }

    function dropdown() {
        var pageMain = doc.querySelector(".page-main");
        var pageFooter = doc.querySelector(".page-main");
        $('[data-uk-dropdown]').on('show.uk.dropdown', function () {
            if (win.innerWidth > 480) {
                overlay2.stop().fadeIn(150, function () {
                    overlay2.addClass("open");
                });
            }
        });
        $('[data-uk-dropdown]').on('hide.uk.dropdown', function () {
            if (win.innerWidth > 480) {
                overlay2.stop().fadeOut(150, function () {
                    overlay2.removeClass("open");
                });
            }
        });
    }

    function carousels() {
        var associates = $(".js-associates-carousel");
        var sertificates = $(".sertificates-gallery__inner");
        var stocks = $(".page-index__stock-list");
        var articleSlider = $(".article__slider");
        var delFlag = false;
        var delFlag2 = true;
        var stocksFlag = true;
        var articleCarouselNext = $(".article__slider.owl-carousel .owl-next");
        var articleCarouselPrev = $(".article__slider.owl-carousel .owl-prev");

        function doIt2() {
            if (win.innerWidth <= 480) {
                delFlag2 = false;
            }
            if (win.innerWidth > 480) {
                delFlag2 = true;
            }
            if (win.innerWidth <= 480 && !associates.hasClass("owl-carousel") && !delFlag2) {
                createSerfs();
                sertificates.addClass("owl-carousel");
            } else if (win.innerWidth > 480 && delFlag2) {
                sertificates.trigger("destroy.owl.carousel");
                sertificates.removeClass("owl-carousel");
            }
        }

        function createSerfs() {
            sertificates.owlCarousel({
                items: 1,
                dots: false,
                nav: true,
                autoplay: false,
                loop: true,
                margin: 20
            });
        }

        articleSlider.owlCarousel({
            items: 1,
            dots: false,
            nav: true,
            autoplay: false,
            loop: true,
            margin: 0
        });

        function stocksCreate() {
            stocks.owlCarousel({
                items: 3,
                dots: false,
                nav: true,
                autoplay: false,
                loop: true,
                margin: 40,
                responsive: {
                    0: {
                        items: 1
                    },

                    570: {
                        items: 2
                    },

                    850: {
                        items: 3
                    }
                }
            });
        }

        stocksCreate();

        function create() {
            associates.owlCarousel({
                items: 3,
                dots: false,
                nav: true,
                autoplay: false,
                loop: true,
                margin: 20,
                responsive: {
                    0: {
                        items: 1
                    },

                    570: {
                        items: 2
                    },

                    850: {
                        items: 3
                    },

                    1100: {
                        items: 4
                    }
                }
            });
        }

        function doIt() {
            if (win.innerWidth <= 480) {
                delFlag = true;
            }
            if (win.innerWidth > 480) {
                delFlag = false;
            }
            if (win.innerWidth <= 767 && stocksFlag) {
                stocks.trigger("destroy.owl.carousel");
                stocks.removeClass("owl-carousel");
                stocksFlag = false;
            }
            if (win.innerWidth > 767 && !stocksFlag) {
                stocks.addClass("owl-carousel");
                stocksCreate();
                stocksFlag = true;
            }

            if (win.innerWidth <= 480 && associates.hasClass("owl-carousel") && delFlag) {
                associates.trigger("destroy.owl.carousel");
                associates.removeClass("owl-carousel");
            } else if (win.innerWidth > 480 && !delFlag) {
                create();
                associates.addClass("owl-carousel");
            }
        }

        doIt();
        doIt2();

        function sliderMatchHeight() {
            var activeImg = articleSlider.find(".owl-item.active").find(".article__slider-item-img");
            if (articleCarouselNext.outerHeight() != activeImg.outerHeight()) {
                articleCarouselNext.css({ "height": activeImg.outerHeight() });
                articleCarouselPrev.css({ "height": activeImg.outerHeight() });
            }
        }

        sliderMatchHeight();
        articleSlider.on('translate.owl.carousel', function (event) {
            sliderMatchHeight();
        });
        win.addEventListener("resize", function () {
            associates.trigger('refresh.owl.carousel');
            doIt();
            sliderMatchHeight();
            doIt2();
        });
    }

    function sandwitchDD() {
        var parent = $(".slide-menu__nav-parent");
        var link = $(".slide-menu__nav-parent > a");
        var ddheader = $(".slide-menu__nav-submenu-header");

        link.click(function (event) {
            var dropdown = $(this).siblings(".slide-menu__nav-submenu");
            if (!dropdown.hasClass("active")) {
                dropdown.addClass("active");
            }
            event.preventDefault();
        });
        ddheader.click(function () {
            $(this).parents(".slide-menu__nav-submenu").removeClass("active");
        });
    }

    function mobileCardTitle() {
        var tablet = false;
        var target = $(".b-product__title");
        var mobileContainer = $(".b-product__mobile-title-wrapper");
        var desktopContainer = $(".b-product__desktop-title-wrapper");

        function tabletFlug() {
            if (win.innerWidth <= 959) {
                tablet = true;
            } else if (win.innerWidth > 959) {
                tablet = false;
            }
            if (tablet) {
                target.prependTo(mobileContainer);
            } else if (!tablet) {
                target.prependTo(desktopContainer);
            }
        }

        tabletFlug();
        win.addEventListener("resize", function () {
            tabletFlug();
        });
    }

    function adaptiveSliderSpares() {
        var slider = $(".adaptive-spares-slider");
        var slider2 = $(".adaptive-spares-slider2");
        var itsSliderFlag = false;

        function doIT() {
            if (win.innerWidth <= 479 && !itsSliderFlag) {
                slider.addClass("owl-carousel");
                slider2.addClass("owl-carousel");
                slider.owlCarousel({
                    items: 1,
                    dots: true,
                    nav: true,
                    autoplay: false,
                    loop: true,
                    margin: 0
                });
                slider2.owlCarousel({
                    items: 3,
                    dots: true,
                    nav: false,
                    autoWidth: true,
                    autoplay: false,
                    loop: true,
                    margin: 10
                });
                itsSliderFlag = true;
                slider.trigger('refresh.owl.carousel');
                slider2.trigger('refresh.owl.carousel');
            } else if (win.innerWidth > 479 && itsSliderFlag) {
                slider.trigger("destroy.owl.carousel");
                slider.removeClass("owl-carousel");
                slider2.trigger("destroy.owl.carousel");
                slider2.removeClass("owl-carousel");
                itsSliderFlag = false;
            }
        }

        doIT();
        win.addEventListener("resize", function () {
            doIT();
        });
    }

    function autoHeight() {
        function doIt() {
            if (win.innerWidth > 479 && !autoHeightFlag) {
                $('.spares .spares-item').matchHeight({
                    byRow: true
                });
                $('.spares-item.spares-item--big  .spares-item__description').matchHeight({
                    byRow: false
                });
                $('.spares-item.spares-item--big2  .spares-item__description').matchHeight({
                    byRow: true
                });
                autoHeightFlag = true;
            }
        }

        doIt();
        win.addEventListener("resize", function () {
            doIt();
        });
    }

    function advCarousel() {
        var list = $(".advantages__list");
        var itsSliderFlag = false;

        function doIT() {
            if (win.innerWidth <= 1024 && !itsSliderFlag) {
                list.addClass("owl-carousel");
                itsSliderFlag = true;
                list.owlCarousel({
                    items: 4,
                    dots: true,
                    nav: true,
                    autoplay: false,
                    loop: true,
                    margin: 20,
                    responsive: {
                        0: {
                            items: 1
                        },

                        570: {
                            items: 2
                        },

                        850: {
                            items: 3
                        }
                    }
                });
            } else if (win.innerWidth > 1024 && itsSliderFlag) {
                list.trigger("destroy.owl.carousel");
                list.removeClass("owl-carousel");
                itsSliderFlag = false;
            }
        }

        doIT();
        win.addEventListener("resize", function () {
            doIT();
        });
    }

    function adpImgVideoAdaptive() {
        var target = doc.querySelectorAll(".service-adp-block__img-video");
        var mob = doc.querySelectorAll(".service-adp-block__img-video-mobile");
        var desk = doc.querySelectorAll(".service-adp-block__img-video-desktop");
        var tlenght = target.length;
        var Flag = false;

        function doIT() {
            // if (win.innerWidth <= 1024 && !Flag){
            //     var par = target.parents(".service-adp-block");
            //     var mobileContainer = par.find(".service-adp-block__img-video-mobile");
            //     var desktopContainer = par.find(".service-adp-block__img-video-desktop");
            //     mobileContainer.appendChild(target);
            // }
            if (win.innerWidth <= 1024 && !Flag) {
                for (var i = 0; i < tlenght; i++) {
                    mob[i].appendChild(target[i]);
                }
                Flag = true;
            } else if (win.innerWidth > 1024 && Flag) {
                for (var i = 0; i < tlenght; i++) {
                    desk[i].appendChild(target[i]);
                }
                Flag = false;
            }
        }

        doIT();
        win.addEventListener("resize", function () {
            doIT();
        });
    }

    function headRoom() {
        var elem = doc.getElementById("page-header");
        var headerNavbar = doc.getElementById('header-navbar');
        var $headerNavbar = $('#header-navbar');
        var searchBar = doc.getElementById('search-bar');
        var headerLogo = doc.querySelector('.logo-pannel__logo');
        var phoneHeader = doc.querySelector('.logo-panel__phone');
        var logo = doc.querySelector('.logo-pannel__logo-pic');
        var logoPanel = doc.querySelector('.logo-panel');
        var duration = 200;
        var html = $('html');
        var flag = false;
        var init = true;
        var fl = false;
        var lastCoord = 99999999999;
        var headroom = new Headroom(elem, {
            "offset": 250,
            "tolerance": 5,
            "classes": {
                "initial": "animated",
                "pinned": "slideDown",
                "unpinned": "slideUp"
            },
            onPin: function onPin() {
                Vel(headerNavbar, "slideDown", { duration: duration }, { easing: "none" });
                html.addClass("onPin");
                html.removeClass("onUnpin");
                html.removeClass("onTop");
                html.removeClass("onNotTop");
            },
            onUnpin: function onUnpin() {
                Vel(headerNavbar, "slideUp", { duration: duration }, { easing: "none" }, {});
                flag = true;
                html.removeClass("onPin");
                html.addClass("onUnpin");
                html.removeClass("onTop");
                html.removeClass("onNotTop");
            },
            onTop: function onTop() {
                Vel(logo, { height: "65px" }, { duration: duration }, { easing: "none" }, { complete: function complete() {
                        console.log("complete");
                    } });
                html.removeClass("onPin");
                html.removeClass("onUnpin");
                html.addClass("onTop");
                html.removeClass("onNotTop");
            },
            onNotTop: function onNotTop() {
                html.removeClass("onPin");
                html.removeClass("onUnpin");
                html.removeClass("onTop");
                html.addClass("onNotTop");
                Vel(headerNavbar, "slideUp", { duration: duration }, { easing: "none" }, {});
                Vel(logo, { height: "50px" }, { duration: duration }, { easing: "none" }, {});
                logoPanel.classList.add("logo-panel--pinned");
                function de() {
                    var bodyScrollTop = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop;

                    if (bodyScrollTop > 300 && init === true) {
                        lastCoord = bodyScrollTop;
                        init = false;
                    }
                    if (bodyScrollTop < lastCoord && !fl) {
                        headroom.onPin();
                        fl = true;
                    }
                }

                de();
                win.addEventListener('scroll', function () {
                    de();
                });
            }
        });
        var stickyTarget = $("#article-anchor-nav-desk");

        headroom.init();
    }
    function anchorNav() {
        var $anchorNav = $("#article-anchor-nav");
    }
    win.addEventListener("load", function () {
        $('input[name="phone"]').mask('+7 (999) 999-99-99');
        carousels();
        mobileCardTitle();
        sandwitchDD();
        slideOut();
        anchorNav();
        advCarousel();
        searchBarF();
        dropdown();
        adaptiveSliderSpares();
        autoHeight();
    });
    doc.addEventListener("DOMContentLoaded", function () {
        adpImgVideoAdaptive();
        headRoom();
    });
    var lastTargetOffset = 0;

    $(function () {
        var winScrollLast = 0;
        $('.article__anchor-nav a[href*="#"]:not([href="#"])').click(function () {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                var html = $("html");
                var offset = 0;
                var speed = 600;
                target = target.length ? target : $('[id=' + this.hash.slice(1) + ']');
                if (target.length) {
                    console.log(winScrollLast + " winScrollLast");
                    console.log("______________________________________");
                    console.log(target.offset().top + " offsetTop");
                    console.log(lastTargetOffset + " lastTargetOffset");
                    if (win.innerWidth >= 1024) {
                        if (target.offset().top === lastTargetOffset && winScrollLast === $(win).scrollTop() && winScrollLast !== 0) {
                            return false;
                        }
                        winScrollLast = $(win).scrollTop();
                        if (lastTargetOffset === 0 || target.offset().top <= lastTargetOffset) {
                            offset = 62;
                        }
                        lastTargetOffset = target.offset().top;
                        $('html, body').animate({
                            scrollTop: target.offset().top - offset
                        }, speed);
                        return false;
                    } else if (win.innerWidth < 1024) {
                        $('html, body').animate({
                            scrollTop: target.offset().top - $(".mobile-navbar").outerHeight()
                        }, 600);
                        return false;
                    }
                }
            }
        });
    });
})(window, document, window.jQuery);
;(function () {
    document.addEventListener("DOMContentLoaded", function() {

        // Open-close sections
        var editbtn1 = document.querySelector(".checkout__title-edit--text");
        var editbtn2 = document.querySelector(".checkout__title-edit--arrow");
        var section = document.querySelector(".checkout__section");
        if (!editbtn1 && !editbtn2) return;
        if (!section) return;

        editbtn1.addEventListener("click", function (event) {
            event.preventDefault();
            section.classList.toggle("checkout__section--close");
            section.classList.toggle("checkout__section--open");
        });
        editbtn2.addEventListener("click", function (event) {
            event.preventDefault();
            section.classList.toggle("checkout__section--close");
            section.classList.toggle("checkout__section--open");
        });

        // Section delivery
        var choose1      = document.querySelector(".checkout__choose-delivery-method--self");
        var choose2      = document.querySelector(".checkout__choose-delivery-method--courier");
        var choose2Input = document.querySelector("#deliveryMethodCourier");
        var choose3      = document.querySelector(".checkout__choose-delivery-method--transport");

        var subsection1 = document.querySelector(".checkout__self");
        var subsection2 = document.querySelector(".checkout__courier");
        var subsection3 = document.querySelector(".checkout__transport");

        if (!choose1 || !choose2 || !choose3 || !subsection1 || !subsection2 || !subsection3) return;
        if (!choose2Input) return;
        subsection1.classList.add("checkout__self--open");

        choose1.addEventListener("click", function (event) {
            subsection1.classList.add("checkout__self--open");
            subsection2.classList.remove("checkout__courier--open");
            subsection3.classList.remove("checkout__transport--open");

            // control border
            choose1.classList.add   ("checkout__choose-delivery-method--right-border-green");
            choose3.classList.remove("checkout__choose-delivery-method--left-border-green");
        });
        choose2.addEventListener("click", function (event) {
            subsection1.classList.remove("checkout__self--open");
            subsection2.classList.add("checkout__courier--open");
            subsection3.classList.remove("checkout__transport--open");

            // control border
            choose1.classList.add   ("checkout__choose-delivery-method--right-border-green");
            choose3.classList.add   ("checkout__choose-delivery-method--left-border-green");
        });
        choose3.addEventListener("click", function (event) {
            subsection1.classList.remove("checkout__self--open");
            subsection2.classList.remove("checkout__courier--open");
            subsection3.classList.add("checkout__transport--open");

            // control border
            choose1.classList.remove   ("checkout__choose-delivery-method--right-border-green");
            choose3.classList.add      ("checkout__choose-delivery-method--left-border-green");
        });

        // Add comment
        // Section delivery
        var addComment      = document.querySelector(".checkout__courier-addcomment");
        var CommentCourier = document.querySelector(".checkout__textarea--courier-comment");

        if (!addComment) return;
        if (!CommentCourier) return;

        addComment.addEventListener("click", function (event) {
            event.preventDefault();
            addComment.classList.add        ("checkout__courier-addcomment--hide");
            CommentCourier.classList.remove ("checkout__textarea--hide");
        });
    });
})();
;(function () {
    document.addEventListener("DOMContentLoaded", function() {

        // SELECT
        var formSelect = document.querySelector(".form__select--theme_grey");
        var formField  = document.querySelector(".form__select-field");
        var formItem  = document.querySelector(".form__select-item");
        var formList  = document.querySelector(".form__select-list");
        if (!formSelect) return;
        if (!formField) return;
        if (!formList) return;
        if (!formItem) return;
        formSelect.addEventListener("click", function (event) {
            event.preventDefault();
            formSelect.classList.toggle("form__select--open");
            event = event || window.event;
            var el = event.target || event.srcElement;
            if (formField === el) return;
            formField.value = el.innerHTML;
        });
        // formItem.addEventListener("click", function (event) {
        //     console.log(formField.value);
        //     event.preventDefault();
        //     formSelect.classList.toggle("form__select--open");
        //     event = event || window.event;
        //     var el = event.target || event.srcElement;
        //     formField.value = el.innerHTML;
        //
        // });
    });
})();